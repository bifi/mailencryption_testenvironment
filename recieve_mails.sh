#!/bin/bash

#
# example usage: ./recieve_mails.sh noencryption.test pop3
#

# check for enough arguments

if [ $# -lt 2 ]
then
    echo "Not enough arguments supplied"
    exit 1
fi

## Variables Section

HOST=$1
USER=`echo $1 | cut -d. -f1`
PROTO=$2
TIMES=100

MAILBOX_FILE=`echo ~/mastermails/$USER\_$PROTO.mbox`
GETMAIL_CONF=`echo $USER\_$PROTO`
CUR_LOC=`pwd`

GETMAIL_DIR=.getmail_master
GETMAIL_CONF_DIR="$CUR_LOC/getmail_configurations"

# create directories

if [ ! -d "~/$GETMAIL_DIR" ]; then
  mkdir ~/$GETMAIL_DIR
fi

if [ ! -d "~/mastermails" ]; then
  mkdir ~/mastermails
fi

# add SSH key to SSH agent
ssh-add $CUR_LOC/keys/vagrant.id_ecdsa >/dev/null 2>&1

echo ""
echo "TEST on $HOST"

# remote:  generate mails on remote mail server
ssh -q -t vagrant@$HOST './generate_mails.sh' 2>&1 > /dev/null

for ((i=1;i<=$TIMES;i++));
do

	# create local mailbox file
    touch $MAILBOX_FILE

	# get execution time of mail download
    DURATION=`(time getmail -g $GETMAIL_CONF_DIR -r "$GETMAIL_CONF") 2>&1 | grep real `

	# convert output of time from minutes to seconds
    echo $DURATION | cut -d ' ' -f 2 | sed 's/m/:/' | cut -d \. -f1 | sed s/:/*60+/g | sed s/s//g | sed s/,/./g  | bc

	# remove local mailbox
    rm $MAILBOX_FILE

	# wait 5 seconds to not overload the server
    sleep 5
done

# remote delete mailbox on remote server
ssh -q -t vagrant@$HOST 'sudo ./clearinbox.sh' 2>&1 > /dev/null

# remove obsolete directories and files
rm -r ~/$GETMAIL_DIR
rm -r ~/mastermails
rm $GETMAIL_CONF_DIR/oldmail-*

exit 0
