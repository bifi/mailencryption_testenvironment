# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  # =========================================================================
  # The servers in our environment
  # =========================================================================
  servers=[
    {
      :hostname => "monitor.test",
      :vmname => "monitor",
      :box => "debian/stretch64",
      :ip => "192.168.66.10",
      :port => 23000,
      :ram => 2048,
      :cpu => 1
    },
    {
      :hostname => "trees.test",
      :vmname => "trees",
      :box => "debian/stretch64",
      :ip => "192.168.66.11",
      :port => 23001,
      :ram => 2048,
      :cpu => 1
    },
    {
      :hostname => "scrambler.test",
      :vmname => "scrambler",
      :box => "debian/stretch64",
      :ip => "192.168.66.12",
      :port => 23002,
      :ram => 2048,
      :cpu => 1
    },
    {
      :hostname => "mailcrypt.test",
      :vmname => "mailcrypt",
      :box => "debian/stretch64",
      :ip => "192.168.66.13",
      :port => 23003,
      :ram => 2048,
      :cpu => 1
    },
    {
      :hostname => "gpgsieve.test",
      :vmname => "gpgsieve",
      :box => "debian/stretch64",
      :ip => "192.168.66.14",
      :port => 23004,
      :ram => 2048,
      :cpu => 1
    },
    {
      :hostname => "noencryption.test",
      :vmname => "noencryption",
      :box => "debian/stretch64",
      :ip => "192.168.66.15",
      :port => 23005,
      :ram => 2048,
      :cpu => 1
    },
    {
      :hostname => "imaptester.test",
      :vmname => "imap",
      :box => "debian/stretch64",
      :ip => "192.168.66.16",
      :port => 23006,
      :ram => 2048,
      :cpu => 1
    }
  ]

  # =========================================================================
  # General Settings
  # =========================================================================
  config.vm.provider "virtualbox" do |v|
    v.linked_clone = true
  end

  # do not sync anything to vm (no need for nfs)
  config.vm.synced_folder '.', '/vagrant', disabled: true

  # SSH settings
  config.ssh.insert_key = false
  config.ssh.private_key_path = ["keys/vagrant.id_ecdsa", "~/.vagrant.d/insecure_private_key"]
  config.vm.provision "file", source: "keys/vagrant.id_ecdsa.pub", destination: "~/.ssh/authorized_keys"

  # =========================================================================
  # Machine specific settings
  # =========================================================================
  servers.each do |machine|
    config.vm.define machine[:hostname] do |node|
      node.vm.box = machine[:box]
      node.vm.hostname = machine[:hostname]
      node.vm.network "private_network", ip: machine[:ip]
      node.vm.network "forwarded_port", guest: 22, host: machine[:port], id: "ssh"

      if machine[:hostname] == "monitor.test"
            node.vm.network "forwarded_port", guest: 3000, host: 3000, id: "grafana"
            node.vm.network "forwarded_port", guest: 9090, host: 9090, id: "prometheus"
      end

      node.vm.provider "virtualbox" do |vb|
        vb.memory = machine[:ram]
        vb.cpus = machine[:cpu]
		vb.name = machine[:vmname]
      end

      # Only execute once the Ansible provisioner,
      # when all the machines are up and ready.
      if machine[:hostname] == "imaptester.test"
		config.vm.provision :ansible do |ansible|
			# Disable default limit to connect to all the machines
			ansible.compatibility_mode = "2.0"
			ansible.limit = "all"
			ansible.inventory_path = "ansible/hosts"
			# ansible.galaxy_role_file = "ansible/requirements.yml"
			ansible.playbook = "ansible/playbook.yml"
			ansible.raw_arguments = Shellwords.shellsplit(ENV['ANSIBLE_ARGS']) if ENV['ANSIBLE_ARGS']
        end
      end
    end
  end
end
