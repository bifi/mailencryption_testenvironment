#!/bin/bash

times=$1
test=$2
host=$3

for ((i=1;i<=$times;i++));
do

ansible all -i ansible/hosts --private-key=/home/paul/Desktop/Masterarbeit/07_Testumgebung/keys/vagrant.id_ecdsa -u vagrant -l $host -m shell -a '/bin/bash -c'"$test"''

# Clear INBOX

#echo "Clearing INBOX"
#ansible all -i ansible/hosts --private-key=/home/paul/Desktop/Masterarbeit/07_Testumgebung/keys/vagrant.id_ecdsa -u vagrant -l $host -m shell -a '/bin/bash -c "sudo ./clearinbox.sh"'

sleep 5

done
