require 'sqlite3'
require 'openssl'
require 'bcrypt'
require 'base64'

class Database

   def initialize(name)
      @name = name.capitalize
   end
   def create_encryption_keys
#      puts default_private_key_password
      puts "#{default_private_key_iterations}$#{default_private_key_salt}$#{default_public_key}$#{default_private_key}"
   end


  def default_public_key
    escape_pem default_key_pair.public_key.to_pem
  end

  def default_private_key(password = nil)
    settings = '$2a$%02u$%22s' % [ default_private_key_iterations, default_private_key_salt ]
    hashed_password = BCrypt::Engine.hash_secret (password || default_private_key_password), settings
    escape_pem default_key_pair.to_pem(OpenSSL::Cipher.new('aes-256-cbc'), hashed_password)
  end

  def default_private_key_password
    'PASSWORD'
  end

  def default_private_key_salt
    @salt ||= Base64.strict_encode64(random.bytes(16)).gsub('+', '.').slice 0, 22
  end

  def default_private_key_iterations
    12
  end

  def default_key_pair
    @key_pair ||= OpenSSL::PKey::RSA.new 2048
  end

  def escape_pem(pem)
    pem.gsub /\n/, '_'
  end

  def random
    @random ||= Random.new
  end

end

hello = Database.new("World")
hello.create_encryption_keys
