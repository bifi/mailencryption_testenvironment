#!/bin/bash
#
# example usage: ./run_test_on_x_hosts.sh 10 "time ./diskspace_usage_after_encryption.sh" "noencryption.test mailcrypt.test scrambler.test trees.test"
#

times=$1
test=$2
hosts=$3

for host in $hosts;
do

echo "########### $host: "

./run_test_x_times.sh $times "$2" $host

done

notify-send -i face-laugh "Test is Done" "Test is done"
