# Test environment for personal encrypted mail storage plugins

This vagrant setup creates a test environment for personal encrypted mail 
storage plugins. Personal encrypted mail storage plugins encrypt incoming for the
individual user. After encryption, those plugins ensure that the 
mail server cannot decrypt the mail by itself. To decrypt an encrypted mail 
the password of the user is necessary.  

## Setup

Therefore it spins up seven virtual machines with Debian 9 and
provisions them with Ansible. 

The following machines will be created:
  * **noencryption.test**
    * Mail server ([postfix](http://www.postfix.org/) + [dovecot](https://www.dovecot.org/)) without any encryption for reference purposes 
  * **gpgsieve.test**
    * Mail server with [GPG-Sieve-Filter](https://github.com/EtiennePerot/gpgit) encryption
  * **mailcrypt.test**
    * Mail server with [MailCrypt](https://wiki.dovecot.org/Plugins/MailCrypt) encryption (encrypted folder key mode)
  * **scrambler.test**
    * Mail server with [Scrambler](https://github.com/posteo/scrambler-plugin) encryption
  * **trees.test**
    * Mail server with [TREES](https://0xacab.org/riseuplabs/trees) encryption
  * **imaptester.test**
    * Server with [IMAPTest](https://imapwiki.org/ImapTest) software
  * **monitoring.test**
    * Monitoring Server with [Prometheus](https://prometheus.io) and [Grafana](https://grafana.com/)


## Requirements

To use this environment you have to meet the following hard- and 
software-requirements:

Hardware:
  * a machine with around 10GB Ram

Software:
  * [Ansible](https://www.ansible.com/) (>= 2.4)
  * [VirtualBox](https://www.virtualbox.org/) 
  * [Vagrant](https://www.vagrantup.com/)

## Hostnames

Either you install `vagrant-hostsupdater` via `vagrant plugin install vagrant-hostsupdater` command or add the following entries to your `/etc/hosts`

```
192.168.66.10 monitor.test     
192.168.66.11 trees.test       
192.168.66.12 scrambler.test   
192.168.66.13 mailcrypt.test   
192.168.66.14 gpgsieve.test    
192.168.66.15 noencryption.test
192.168.66.16 imaptester.test  
```  

## Usage

Initial Setup:

```
git clone https://gitlab.com/bifi/mailencryption_testenvironment 
cd mailencryption_testenvironment
vagrant up
```

Reprovisioning:

```
vagrant provision
```


## What is where? 

### Usage 

After a successful provision you can login to every mail account with a common
mail client. Be sure to accept it's snakeoil certificate. 

#### Credentials: 

Use the following credentials to login with a mail client. Be sure to use 
username@server.test as login scheme: 

```
Username: "{{ mariadb_mailuser }}@{{ hostname }}"  
# e.g. trees@trees.test, gpgsieve@gpgsieve.test, noencryption@noencryption.test, ...
Password: PASSWORD 
```

#### Monitoring: 

##### Prometheus

The Prometheus interface can be found at `localhost:9090`.

##### Grafana Dashboards

The Grafana Dashboards can be found at `localhost:3000`. You have to use the
following credentials: 

```
Username: admin 
Password: admin 
```

Then you have to click on `Home > General > Testbench` Node Metrics and you can
see the live node metrics. 
